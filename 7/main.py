#Made by Zhirong Huang

import sys, os;
from operator import itemgetter

def getEdges(G, n):
	retList = [];
	for i in G:
		if i[0] == n or i[1] == n:
			retList.append(i);
	return retList;

def checkEdges(MST, node):
	nodeList = [];
	for i in MST:
		if i[0] not in nodeList:
			nodeList.append(i[0]);
		if i[1] not in nodeList:
			nodeList.append(i[1]);
	if node[0] in nodeList and node[1] in nodeList:
		return True;
	return False;

def prime(G, current_node):
	totalNode = G[0][0];
	graph = G[1:];
	MST = [];
	nodeList = [];
	nodeList.append(current_node);
	print("Starting Node: " + str(current_node));
	for i in range(0, totalNode-1):
		targetNodeList = [];
		for j in nodeList:
			tempList = getEdges(graph, j)
			for k in tempList:
				targetNodeList.append(k);
		sortedNodeList = sorted(targetNodeList, key=itemgetter(2));
		for j in sortedNodeList:
			if len(MST) == 0 or not checkEdges(MST, j):
				if j[0] not in nodeList:
					current_node = j[0];
				else:
					current_node = j[1];
				MST.append(j);
				nodeList.append(current_node);
				print("Added " + str(current_node));
				print("Using Edge " + str(j));
				break;

def kruskal(G):
	totalNode = G[0][0];
	graph = G[1:];
	MST = [];
	targetNodeList = [];
	nodeList = [];
	for i in range(0, totalNode-1):
			tempList = getEdges(graph, i)
			for j in tempList:
				targetNodeList.append(j);
	sortedNodeList = sorted(targetNodeList, key=itemgetter(2));
	for i in range(0, totalNode-1):
		for j in sortedNodeList:
			if len(MST) == 0 or not checkEdges(MST, j):
				if j[0] not in nodeList:
					nodeList.append(j[0]);
				if j[1] not in nodeList:
					nodeList.append(j[1]);
				MST.append(j);
				print("Select Edge " + str(j));

def printHelp():
	print("""Commands: 
exit or ctrl-d - quits the program
help - prints this menu
prim integer_value - run's Prim's algorithm starting at node given
kruskal - runs Kruskal's algorithm""");

def readFile(fileName):
	file = open(fileName, "r");
	temp_lines = file.readlines();
	file.close();
	lines = [];
	for i in range(0, len(temp_lines)):
		temp_lines[i] = temp_lines[i].replace('\n', '');
		temp_split = temp_lines[i].split(' ');
		for j in range(0, len(temp_split)):
			temp_split[j] = int(temp_split[j]);
		lines.append(temp_split);
	return lines;

def main():
	print("Give the file name graph is in: ");
	fileName = input();
	lines = readFile(fileName);
	printHelp();
	while(True):
		print("Enter command: ");
		cmd = input();
		if "exit" in cmd:
			print("Bye");
			sys.exit(0);
		elif "help" in cmd:
			printHelp();
		elif ("kruskal" in cmd):
			print("Running Kruskal's Algorithm");
			kruskal(lines);
		elif "prime" in cmd:
			start = int(cmd.replace('prime ', ''));
			print("Running Prim's Algorithm");
			prime(lines, start);


print("Welcome to Minimum Spanning Tree Finder");
main();
