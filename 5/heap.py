#Made by Zhirong Huang

class heap():
        def __init__(self):
                self.h = [];
                self.currentSize = 0;

        def __str__(self):
                temp = [str(_) for _ in self.h]
                return " ".join(temp);

        def makenull(self):
                self.h = [];

        def insert(self, x):
                self.h.append(x);
                self.currentSize += 1;
                if self.currentSize == 1:
                        return;
                self.upheap(self.currentSize-1);

        def parent(self, i):
                return self.h[i // 2];

        def left(self, i):
                return self.h[(i+1)*2 - 1];

        def right(self, i):
                return self.h[(i+1)*2];

        def swap(self, a, b):
                temp = self.h[a];
                self.h[a] = self.h[b];
                self.h[b] = temp;

        def upheap(self, i):
                while i // 2 >= 0:
                        if self.h[i] < self.h[i // 2]:
                                self.swap(i, i // 2);
                        i = i // 2
                        if i == 0:
                                break;

        def downheap(self, i):
                while (i * 2) <= self.currentSize-1:
                        mc = self.minChild(i)
                        if self.h[i] > self.h[mc]:
                                self.swap(i, mc);
                        i = mc

        def minChild(self,i):
            if (i * 2 + 1) > self.currentSize:
                return i * 2;
            else:
                if self.h[i*2] < self.h[i*2+1]:
                    return i * 2
                else:
                    return i * 2 + 1

        def inorder(self, i):
                print(self.h);

        def preorder(self, i):
                l = self.h[i:];
                return sorted(l);

        def postorder(self, i):
                l = self.h[i:];
                print(sorted(l, reverse=True));

        def min(self):
                print(min(self.h));

        def deletemin(self):
                self.h[0] = self.h[self.currentSize-1];
                self.currentSize -= 1;
                self.h.pop();
                self.downheap(0)
