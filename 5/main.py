from heap import *
import sys

def message():
	print("""Welcome to the Heap
The List of Commands is below, type help to see them again.
help - Prints this list
makenull - Clears the heap
insert <integer> - Inserts the number into the heap
min - Prints the current min on the heap
inorder - Prints heap in inorder
preorder - Prints heap in preorder
postorder - Prints heap in postorder
deletemin - Removes min from the heap
sort - Calls deletemin repeatedly to print out sorted numbers
exit - Exits the program (also Crtl-D exits)
""")

def main():
	message();
	obj = heap();
	while True:
		s = input();
		if s == "help":
			message();
		elif s == "makenull":
			obj.makenull();
		elif "insert " in s:
			s = s.replace('insert ', '');
			obj.insert(int(s));
		elif s == "min":
			obj.min();
		elif s == "inorder":
			obj.inorder(0);
		elif s == "preorder":
			print(obj.preorder(0));
		elif s == "postorder":
			obj.postorder(0);
		elif s == "deletemin":
			obj.deletemin();
		elif s == "sort":
			for i in obj.preorder(0):
				print (i);
		elif s == "exit":
			sys.exit(0);
		else:
			print("Bad Command - type help for commands");

main();
