import sys, os;
from operator import itemgetter

def getDistance(G, n):
	retList = [];
	for i in G:
		if i[0] == n:
			retList.append(i);
	return retList;

def checkLoop(retList, node):
	

def printHelp():
	print("""Possible Commands are: 
dijkstra x - Runs Dijkstra starting at node X. X must be an integer
floyd - Runs Floyd's algorithm
help - prints this menu
exit or ctrl-D - Exits the program""");

def readFile(fileName):
	file = open(fileName, "r");
	temp_lines = file.readlines();
	file.close();
	lines = [];
	for i in range(0, len(temp_lines)):
		temp_lines[i] = temp_lines[i].replace('\n', '');
		temp_split = temp_lines[i].split(' ');
		for j in range(0, len(temp_split)):
			temp_split[j] = int(temp_split[j]);
		lines.append(temp_split);
	return lines;

def dijkstra(G, start_node):
	nodes = G[0][0];
	graph = G[1:];
	retList = [];
	nodeList = {};
	nodeList[0] = 0;
	for i in range(1, nodes):
		nodeList[i] = "inf";
	retList.append(nodeList[start_node]);
	for i in range(1, nodes):
		tempNodeList = sorted(getDistance(graph, start_node), key=itemgetter(2));
		minimumDistanct = 0;

	print(nodeList);




def main():
	print("File containing graph: ");
	fileName = input();
	lines = readFile(fileName);
	printHelp();
	while(True):
		print("Enter command: ");
		cmd = input();
		if "exit" in cmd:
			print("Bye");
			sys.exit(0);
		elif "help" in cmd:
			printHelp();
		elif ("dijkstra" in cmd):
			start = int(cmd.replace('dijkstra ', ''));
			print(dijkstra(lines, start));



print("Welcome to Minimum Spanning Tree Finder");
main();