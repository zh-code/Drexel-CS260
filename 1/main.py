"""
Programmer: Zhirong Huang
Date: Jun 27 2017.
Course: CS 260 Data Structure

Description: Assignment 1 main function which print message and call function in palindrome.py to check.
"""

import os, sys
from palindrome import check

print ("Welcome to Palindrome Checker!\nEnter a word. The program will tell you if it is a palindrome.\nTo quit enter a blank line.");
while True:
	s = input("Enter word to check:");
	if s == "":
		sys.exit();
	else:
		if check(s):
			print ("The word is a palindrome: True");
		else:
			print ("The word is a palindrome: False");

