"""
Programmer: Zhirong Huang
Date: Jun 27 2017.
Course: CS 260 Data Structure

Description: Function file associated with main.py for Assignment 1
"""
from queue import *

def check(word):
    word = word.strip();
    word = word.lower();
    return word == word[::-1];

if __name__ == '__main__':
	q = Queue();
	q.put("undertakes");
	q.put("impassibly");
	q.put("pop");
	q.put("misericordia");
	q.put("pup");
	q.put("dinars");
	q.put("misprisions");
	q.put("tot");
	count = 0;
	size = q.qsize();
	message = "";
	while q.qsize() != 0:
		if check(q.get()):
			count += 1;
	if count == size:
		message = "Success!";
		print ("Success! Passed " + str(count) + "/" + str(size) + " tests.");
	else:
		message = "Failed! ";

	print (message + "Passed " + str(count) + "/" + str(size) + " tests.");
        
