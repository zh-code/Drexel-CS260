#Made by Zhirong Huang

import math, sys

count = 0;
fib = [];

class Node:
	def __init__(self, num, val):
		self.number = num;
		self.value = val;

def check(num, value):
	global fib;
	check = False;
	for item in fib:
		if item.number == num:
			check = True;
			break;
	if not check:
		fib.append(Node(num, value))

def add(a, b):
	global count;
	count += 1;
	return a + b;

def fib_closed(n):
	value = ((1 + math.sqrt(5))**n - (1 - math.sqrt(5))**n )/(2**n * math.sqrt(5));
	return int(value);

def fib_classic(n):
	global count;
	if n == 0:
		return 0;
	elif n == 1:
		return 1;
	else:
		return add(fib_classic(n-2), fib_classic(n-1));

def fib_loop(n):
	a = 0;
	b = 1;
	for i in range(0, n):
		temp = a;
		a = b;
		if i != (n - 1):
			b = add(temp, b);
	return a;

def fid_mem(n):
	global fib, count;
	for item in fib:
		if item.number == n:
			return item.value;
	if n == 0:
		return 0;
	elif n == 1:
		return 1;
	else:
		value = add(fid_mem(n-1), fid_mem(n-2));
		check(n, value);
		return value;

def printMessage():
	print("""Welcome to the Fibonacci Test Program
To exit, enter a negative number.""")

def main():
	global count;
	printMessage();
	while (True):
		count = 0;
		print("Enter Fibonacci Number to compute:");
		num = int(input());
		if num < 0:
			sys.exit();
		else:
			print("--------------------");
			print ("The closed form finds: " + str(fib_closed(num)));
			count = 0;
			print ("The recursive definition finds: " + str(fib_classic(num)));
			print ("Additions needed for recursive definition: " + str(count));
			count = 0;
			print ("The loop definition finds: " + str(fib_loop(num)));
			print ("Additions needed for loop definition: " + str(count));
			count = 0;
			print ("The memoization definition finds: " + str(fid_mem(num)));
			print ("Additions needed for memorization definition: " + str(count));


main();
