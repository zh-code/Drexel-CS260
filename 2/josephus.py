#josephus.py by Zhirong Huang

import sys
from node import *
from queue import *

if (len(sys.argv) != 3):
	print ("Usage: python josephus NumPeople MthPerson");
	sys.exit(0);

def main():
	Q = Queue();
	res = ""; # return string
	step = int(sys.argv[2])-1; #count self as one so the one going to be eliminated is current position + M - 1
	for x in range(0, int(sys.argv[1])): # enqueue for all
		Q.enqueue(x);
	position = Q.item; # go to last one set next one to the first one
	while (position.getNext() != None):
		position = position.getNext();
	position.setNext(Q.item);
	while (position.getNext() != None): # start from first one
		for i in range(0, step):
			position = position.getNext(); #Postion before elimination
		res+= str(position.getNext()); # add to return string
		if (str(position) == str(position.getNext())): # avoid inf loop
			break; 
		position.setNext(position.getNext().getNext()); # eliminate one set next to the one after
	print(res); 

if (__name__ == "__main__"):
	main();
