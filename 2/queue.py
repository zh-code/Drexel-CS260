#queue.py by Zhirong Huang

from node import *

class Queue:
	def __init__(self):
		self.item = None;

	def __str__(self):
		if (self.item == None):
			return "Queue Empty";
		else:
			start = self.item;
			result="";
			while (start != None):
				result+=str(start);
				start=start.getNext();
			return result;

	def front(self):
		return self.item.value;

	def empty(self):
		if (self.item == None):
			return True;
		else:
			return False;

	def enqueue(self, x):
		n = Node(x, None);
		if (self.item == None):
			self.item = n;
		else:
			position = self.item;
			while (position.getNext() != None):
				position = position.getNext();
			position.setNext(n);

	def dequeue(self):
		self.item = self.item.getNext();

