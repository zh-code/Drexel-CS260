#Made by Zhirong Huang

import os, sys, math

count = 0;

def main():
        global count;
        while(True):
                s = input("Command: ");
                if s == "help":
                        help();
                elif s == "exit":
                        sys.exit();
                else:
                        stdin = s.split(' ');
                        if stdin[0] == "bubblesort":
                                bubblesort(stdin[1:]);
                        elif stdin[0] == "insertion":
                                insertion(stdin[1:]);
                        elif stdin[0] == "mergesort":
                                arr = mergesort(stdin[1:]);
                                print(arr);
                                print ("Comparisons: " + str(count));
                                print ("Approx Min: " + str(math.ceil(math.log(math.factorial(len(stdin[1:])),2))));
                        elif stdin[0] == "quicksort":
                                count = 0;
                                arr = quicksort(stdin[1:]);
                                print ("Comparisons: " + str(count));
                                print ("Approx Min: " + str(math.ceil(math.log(math.factorial(len(stdin[1:])),2))));
                        

def quicksort(arr):
        global count
        less = [];
        equal = [];
        greater = [];
        if len(arr) > 1:
                pivot = arr[len(arr) - 1];
                for i in range(0, len(arr)):
                        if arr[i] < pivot:
                                less.append(arr[i]);
                        elif arr[i] == pivot:
                                equal.append(arr[i]);
                        elif arr[i] > pivot:
                                greater.append(arr[i]);
                        count += 1;
                return quicksort(less)+equal+quicksort(greater)
        else:
                return arr;

def mergesort(arr):
        if len(arr) <= 1:
                return arr;
        middle = int(len(arr) / 2);
        left = mergesort(arr[:middle]);
        right = mergesort(arr[middle:]);
        return merge(left, right);
                        
def merge(left, right):
        global count;
        ret = [];
        i = 0;
        j = 0;
        while i < len(left) and j < len(right):
                count += 1;
                if left[i] <= right[j]:
                        ret.append(left[i]);
                        i += 1;
                else:
                        ret.append(right[j]);
                        j += 1;
        ret += left[i:];
        ret += right[j:];
        return ret; 

def insertion(arr):
        count = 0;
        for _i in range(1, len(arr)):
                _j = _i;
                while _j > 0:
                        if int(arr[_j-1]) > int(arr[_j]):
                                temp = arr[_j-1];
                                arr[_j-1] = arr[_j];
                                arr[_j] = temp;
                        _j -= 1;
                        count += 1;
        print(arr);
        print("Comparisons: " + str(count));
        print("Approx Min: " + str(math.ceil(math.log(math.factorial(len(arr)),2))));

def bubblesort(arr):
        count = 0;
        swap = True;
        while (swap):
                swap = False;
                for _i in range(0, len(arr)-1):
                        if int(arr[_i]) > int(arr[_i+1]):
                                temp = arr[_i];
                                arr[_i] = arr[_i+1];
                                arr[_i+1] = temp;
                                swap = True;
                        count += 1;
        print(arr);
        print("Comparisons: " + str(count));
        print("Approx Min: " + str(math.ceil(math.log(math.factorial(len(arr)),2))));

def help():
        print("""Commands:
        help - Prints this menu
        exit or CTRL-D - Exits the program
        sort_method int_list - Enter a sort method followed by a list of space sperated integers to sort them
        Possible Sort Methods: bubblesort insertion mergesort quicksort""");

print ("""Welcome to the sorting thunderdome
        This program is used to compare sorting methods""")
help();
main();
